package com.avalon.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import com.avalon.servershop.ServerShop;

public class LanguageUtil {
	
	
	public LanguageUtil() {
		loadLang();
	}
	
	
	public void loadLang() {
		File lang = new File(ServerShop.getInstance().getDataFolder(), "lang.yml");
		if (!lang.exists()) {
			try {
				ServerShop.getInstance().getDataFolder().mkdir();
				lang.createNewFile();
				if (lang != null) {
					YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(lang);
					defConfig.save(lang);
					Lang.setFile(defConfig);
				}
			} catch (IOException e) {
				ServerShop.getInstance().log.warning("�cCouldn't create language file.");
				Bukkit.getPluginManager().disablePlugin(ServerShop.getInstance());
			}
		}
		
		YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
		for (Lang item : Lang.values()) {
			if (conf.getString(item.getPath()) == null) {
				conf.set(item.getPath(), item.getDefault());
			}
		}
		Lang.setFile(conf);
		try {
			conf.save(lang);
		} catch (IOException e) {
			ServerShop.getInstance().log.warning("�cFailed to save lang.yml.");
			ServerShop.getInstance().log.warning("�cReport this stack trace to crysis992.");
			e.printStackTrace();
		}
	}
}
